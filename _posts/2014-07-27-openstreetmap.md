---
layout: post
title: "ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പ്"
author: primejyothi
category: misc
---
വിക്കിപീഡിയ പോലെ ആര്‍ക്കും എഡിറ്റ് ചെയ്യാവുന്ന ഒരു മാപ് ആണ് ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പ്. ഒരു റോഡ് അല്ലെങ്കില്‍ സ്ഥാപനം എന്നിവയെക്കുറിച്ച് ഉപയോഗപ്രദമായ വളരെയധികം വിവരങ്ങള്‍ ഓപ്പണ്‍സ്റ്റ്രീറ്റ്മാപ്പില്‍ ചേര്‍ക്കുവാന്‍ കഴിയും. ആ വിവരങ്ങള്‍ എങ്ങനെയെല്ലാം ഉപയോഗിക്കാം എന്നുള്ളതിനെക്കുറിച്ചാണ് ഇവിടെ പറയാന്‍ പോകുന്നത്. ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പിലെ ഡേറ്റ, സന്നദ്ധപ്രവര്‍ത്തകര്‍ ചേര്‍ക്കുന്നതുകൊണ്ട് മിക്കവാറും ശരിയായിരിക്കാനാണ് സാധ്യത. എന്നിരുന്നാലും ചില സ്ഥലങ്ങളിലെ കവറേജ് മറ്റു മാപ്പുകളെപ്പോലെ പൂര്‍ണ്ണമായെന്നിരിക്കില്ല. അതിനാല്‍ മറ്റേതു സര്‍വീസും ഉപയോഗിക്കുമ്പോഴെടുക്കുന്ന മുന്‍കരുതലുകള്‍ ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പ് ഉപയോഗിക്കുമ്പോഴും എടുക്കേണ്ടതാണ്.

{% include image.html img="images/Openstreetmap_logo.png" caption="ഓപ്പൺസ്ട്രീറ്റ്മാപ്പിന്റെ ലോഗോ" %}

[http://www.openstreetmap.org](http://www.openstreetmap.org) അല്ലെങ്കിൽ [http://osm.org](http://osm.org) ഉപയോഗിച്ച് ഓപ്പണ്‍ സ്ട്രീറ്റ് മാപ്പ് സൈറ്റിലേക്ക് പോകാന്‍ പറ്റും. ഈ സൈറ്റ് പ്രധാനമായും മാപ് എഡിറ്റ് ചെയ്യാനും, ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പ് എങ്ങനെ ഉപയോഗിക്കാം എന്നുള്ള ഒരു ഉദാഹരണവുമാണ്. അക്കാരണത്താല്‍ മറ്റേതെങ്കിലും മാപ്പ് ഉപയോഗിക്കുന്നതുപോലെ ഓപ്പണ്‍സ്ട്രീറ്റ് മാപ്പ് സൈറ്റ് ഉപയോഗിക്കാന്‍ ശ്രമിച്ചാല്‍ ചിലപ്പോള്‍ നിങ്ങള്‍ നിരാശരായേക്കും. ഓപ്പണ്‍സ്ട്രീറ്റ്മാപ്പിലെ വിവരങ്ങള്‍ വളരെ ലളിതമായ ലൈസന്‍സില്‍ ലഭ്യമാണ്. അതിനാല്‍ അതുപയോഗിച്ചിട്ടുള്ള വളരെയധികം സര്‍വീസുകളും ആപ്ലിക്കേഷനുകളും  ഇന്ന് ലഭ്യമാണ്. ഈ സര്‍വീസുകള്‍ അല്ലെങ്കില്‍ ആപ്പ്ലിക്കേഷനുകളുപയോഗിച്ച് നമുക്ക് ആവശ്യമുള്ള വിവരങ്ങള്‍ കണ്ടുപിടിക്കാന്‍ കഴിയും. അങ്ങനെയുള്ള ചില സര്‍വീസുകളും ആപ്പ്ലിക്കേഷനുകളുമാണ് ഇനി വിവരിക്കാന്‍ പോകുന്നത്.

### ഓപ്പണ്‍സ്റ്റ്രീറ്റ്മാപ്പ് ഉപയോഗിച്ചുള്ള നാവിഗേഷൻ

OsmAnd : <a title="https://play.google.com/store/apps/details?id=net.osmand" href="https://play.google.com/store/apps/details?id=net.osmand">https://play.google.com/store/apps/details?id=net.osmand</a>
ഓഫ്‌‌ലൈന്‍ മാപ് സൗകര്യമുള്ള നല്ലൊരു ആന്‍ഡ്രോയ്ഡ് ആപ്ലിക്കേഷനാണിത്. ആവശ്യമുള്ള രാജ്യത്തിന്റെ മാപ് ഡൗണ്‍ലോഡ് ചെയ്താല്‍ ഡേറ്റാ കണക്ഷനില്ലാതെ ഈ ആപ്പുപയോഗിച്ച് നാവിഗേറ്റ് ചെയ്യാം. ഇന്ത്യന്‍ മാപ് ഏകദേശം 200 എംബിയോളം വരും. പത്തു മാപ്പുകള്‍ മാത്രമേ ഫ്രീ വെര്‍ഷനില്‍ ഡൗണ്‍ലോഡ് ചെയ്യാന്‍ പറ്റൂ എന്നൊരു പ്രശ്നമുണ്ട്. ഇതുപയോഗിക്കുന്നതെങ്ങനെ എന്നു വിശദമാക്കുന്ന നല്ലൊരു ബ്ലോഗ് എന്റ്രി ഇവിടെ ഉണ്ട് :<a title="https://joostschouppe.wordpress.com/2014/07/25/using-osmand-on-the-road/" href="https://joostschouppe.wordpress.com/2014/07/25/using-osmand-on-the-road/">https://joostschouppe.wordpress.com/2014/07/25/using-osmand-on-the-road/</a>
navfree : <a title="https://play.google.com/store/apps/details?id=com.navfree.android.OSM.ALL" href="https://play.google.com/store/apps/details?id=com.navfree.android.OSM.ALL">https://play.google.com/store/apps/details?id=com.navfree.android.OSM.ALL</a>
OsmAnd പോലെ ഓഫ്‌‌ലൈന്‍ നാവിഗേഷന്‍ ഉപയോഗിക്കാവുന്ന മറ്റൊരാപ്ലിക്കേഷനാണിത്. ഇതും ഓപ്പണ്‍സ്റ്റ്രീറ്റ് മാപ്പില്‍ നിന്നുള്ള ഡേറ്റയാണുപയോഗിക്കുന്നത്.
ഓണ്‍ലൈന്‍ റൂട്ടിങിനു വേണ്ടി <a title="http://map.project-osrm.org/" href="http://map.project-osrm.org/">http://map.project-osrm.org/</a>, <a title="http://open.mapquest.com/" href="http://open.mapquest.com/">http://open.mapquest.com/</a> എന്നീ സൈറ്റുകള്‍ ഉപയോഗിക്കാവുന്നതാണ്.

### പ്രിന്റഡ് മാപ്പുകൾ
<a title="http://fieldpapers.org/" href="http://fieldpapers.org/">http://fieldpapers.org/</a> എന്ന സൈറ്റില്‍ നിന്ന് ആവശ്യമുള്ള പ്രദേശങ്ങളുടെ മാപ് നമുക്കുതന്നെ സെലക്റ്റ് ചെയ്ത് പിഡിഎഫ് ഫയല്‍ ആയി ഡൗണ്‍ലോഡ് ചെയ്യാന്‍ പറ്റും.  ഓപ്പണ്‍സ്റ്റ്രീറ്റ്മാപ് സൈറ്റില്‍ നിന്നു തന്നെ മാപ്പുകള്‍ jpg/png/pdf/svg ഫോര്‍മാറ്റുകളില്‍ എക്സ്പോര്‍ട്ട് ചെയ്യാനുള്ള സൗകര്യമുണ്ടെങ്കിലും സെര്‍വര്‍ ലോഡ് അധികമാണെങ്കില്‍ റിക്വസ്റ്റ് റിജക്റ്റ് ചെയ്യപ്പെടാനുള്ള സാധ്യതയുണ്ട്. അതിനാല്‍ <a title="fieldpapers.org" href="http://fieldpapers.org/">fieldpapers.org</a> ല്‍ നിന്നും മാപ്പുകള്‍ എടുക്കുന്നതാകും സൗകര്യം.

### മാപ് സെര്‍ച്ച്
നേരത്തേ സൂചിപ്പിച്ചതുപോലെ ഓപ്പണ്‍സ്റ്റ്രീറ്റ് സൈറ്റിന്റെ ഉദ്ദേശം മാപ് എഡിറ്റിങ്ങും മറ്റുമായതിനാല്‍ എല്ലാത്തരം വിവരങ്ങളും ആ സൈറ്റില്‍ കാണാന്‍ പറ്റില്ല. മാത്രവുമല്ല അവിടെ സെര്‍ച്ച് ചെയ്യുന്നത് അത്ര എളുപ്പവുമല്ല. അങ്ങനെയുള്ള ആവശ്യങ്ങള്‍ക്കായി ഉപയോഗിക്കാവുന്ന ചില സൈറ്റുകളാണ് <a title="http://www.openstreetbrowser.org/" href="http://www.openstreetbrowser.org/">http://www.openstreetbrowser.org/</a>, <a title="http://osm24.eu" href="http://osm24.eu">http://osm24.eu</a>, <a title="http://www.lenz-online.de/cgi-bin/osmpoi/osmpoi.pl" href="http://www.lenz-online.de/cgi-bin/osmpoi/osmpoi.pl">http://www.lenz-online.de/cgi-bin/osmpoi/osmpoi.pl</a> എന്നിവ.

### മറ്റു സര്‍വീസുകൾ
* <a title="http://www.openrailwaymap.org/" href="http://www.openrailwaymap.org/">http://www.openrailwaymap.org/</a> : റെയില്‍ മാപ്പുകള്‍.
* <a title="http://openfiremap.org/" href="http://openfiremap.org/">http://openfiremap.org/</a> : ഫയര്‍ സ്റ്റേഷനുകള്‍, ഫയര്‍ ഹൈഡ്രന്റ് എന്നിവയുടെ സ്ഥാനം അടയാളപ്പെടുത്തിയിട്ടുള്ള മാപ്.
* <a title="http://openbeermap.github.io/" href="http://openbeermap.github.io/">http://openbeermap.github.io/</a> : പബ്, ഷാപ്പ് എന്നിവ കാണിക്കാന്‍ വേണ്ടിയുള്ള മാപ്.
* <a title="http://waymarkedtrails.org/en/" href="http://waymarkedtrails.org/en/">http://waymarkedtrails.org/en/</a> : ഹൈക്കിങ് ട്രെയ്‌‌ലുകള്‍
* <a title="http://hot.openstreetmap.org/" href="http://hot.openstreetmap.org/">http://hot.openstreetmap.org/</a> : പ്രകൃതി ദുരന്ത ബാധിത പ്രദേശങ്ങളെ മാപ്പ് ചെയ്യുന്ന പ്രോജക്റ്റ്

ഇത്രയും പറഞ്ഞ സ്ഥിതിക്ക് ഇതില്‍ കുന്നംകുളത്തിന്റെ മാപ്പുണ്ടോ എന്ന ചോദ്യത്തിനും ഉത്തരം തരേണ്ടത് ആവശ്യമാണല്ലോ. കുന്നംകുളത്തിന്റെ മാപ്പുണ്ട്, പക്ഷേ പല വിവരങ്ങളും അതില്‍ ഇപ്പോള്‍ ലഭ്യമല്ല. കുന്നംകുളത്തിന്റെ വിവരങ്ങള്‍ ഓപ്പണ്‍സ്റ്റ്രീറ്റ്മാപ്പില്‍ ചേര്‍ക്കാന്‍ താല്പര്യമുണ്ടെങ്കില്‍ <a title="http://learnosm.org/en/" href="http://learnosm.org/en/">http://learnosm.org/en/</a> എന്ന സൈറ്റില്‍ പോയാല്‍ വിവരങ്ങള്‍ ഓപ്പണ്‍സ്റ്റ്രീറ്റ് മാപ്പില്‍ എങ്ങനെ ചേര്‍ക്കാം എന്നു മനസ്സിലാക്കാം.
