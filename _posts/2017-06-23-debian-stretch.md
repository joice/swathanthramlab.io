---
layout: post
title: "ഡെബീയന്‍ 9 (സ്ട്രെച്ച്) പുറത്തിറങ്ങി"
author: nandakumar
category: news
Tags: ഡെബീയന്‍, debian, distributions, release
---

ഡെബീയന്‍ ഗ്നു/ലിനക്സിന്റെ പുതിയ പതിപ്പ് ഡെബീയന്‍ 9 "സ്ട്രെച്ച്" (Stretch) ജൂണ്‍ 17-ന് പുറത്തിറങ്ങി. ഉബുണ്ടുവടക്കമുള്ള മറ്റു പല ഗ്നു/ലിനക്സ് വിതരണങ്ങളുടെയും അടിസ്ഥാനം ഡെബീയനാണ്. സെര്‍വറുകളിലും ഇതിന് പ്രചാരമേറെയാണ്. പുതിയ പതിപ്പിന്റെ ബന്ധപ്പെട്ട് കേരളത്തിലും റിലീസ് പാര്‍ട്ടികള്‍ സംഘടിപ്പിക്കപ്പെട്ടു.

പ്രൊജക്റ്റ് സ്ഥാപകനായ [ഇയാന്‍ മര്‍ഡോക്കിനുള്ള](https://en.wikipedia.org/wiki/Ian_Murdock) സമര്‍പ്പണം കൂടിയാണ് പുതിയ പതിപ്പ്.

### പുതിയ പതിപ്പിലെ കാര്യമായ മാറ്റങ്ങള്‍:
* ലിനക്സ് കേണല്‍ 4.9-ലേക്ക് അപ്ഗ്രേഡ് ചെയ്യപ്പെട്ടു
* ഫയര്‍ഫോക്സും തണ്ടര്‍ബേഡും ഡെബീയനിലേക്ക് മടങ്ങിയെത്തി
* ഡിഫോള്‍ട്ട് മൈഎസ്ക്യൂഎല്‍ വകഭേദമായി [മരിയാഡിബി](https://en.wikipedia.org/wiki/MariaDB) ഉപയോഗിച്ചുതുടങ്ങി
* മറ്റനേകം പാക്കേജുകള്‍ പുതിയ പതിപ്പുകളിലേക്ക് അപ്ഗ്രേഡ് ചെയ്യപ്പെട്ടു

### ലിങ്കുകള്‍:
* [ഔദ്യോഗിക പത്രക്കുറിപ്പ്](https://www.debian.org/News/2017/20170617)
* [ഡൗണ്‍ലോഡ് പേജ് -- ഏറ്റവും പുതിയ പതിപ്പ്](https://www.debian.org/distrib/)
